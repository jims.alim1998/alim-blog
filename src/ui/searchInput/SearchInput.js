import React from "react";
import { ImSearch } from "react-icons/im";

import "./searchInput.scss";

const SearchInput = () => {
  return (
    <div className="searchInput convert-npt">
      <ImSearch fontSize={18} />
      <input className="searchInput__input no-npt" placeholder="Хайх..." />
    </div>
  );
};

export default SearchInput;
