import React from "react";
import "./pagination.scss";

const Pagination = ({
  totalPage,
  perPage,
  currentPage,
  setCurrentPage,
  setIsLoading,
}) => {
  const _pageCount = Math.ceil(totalPage / perPage);

  const gotoPage = (index) => {
    window.scrollTo({ top: 0 });
    setIsLoading(true);

    setTimeout(() => setIsLoading(false), 2000);

    setCurrentPage(index + 1);
  };

  return (
    <div className="pagination">
      {[...Array(_pageCount)].map((_, idx) => (
        <button
          key={idx}
          className={`pagination-page outline-btn ${
            currentPage === idx + 1 && "active"
          }`}
          disabled={currentPage === idx + 1}
          onClick={() => gotoPage(idx)}
        >
          {idx + 1}
        </button>
      ))}
    </div>
  );
};

export default Pagination;
