import React from "react";
import Header from "../Header/Header";

import "./layout.scss";

const Layout = ({ children, centered = false }) => {
  return (
    <section className="layout">
      <Header />

      <main className={`layout__content ${centered && "centered"}`}>
        {children}
      </main>
    </section>
  );
};

export default Layout;
