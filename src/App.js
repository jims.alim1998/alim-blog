import React, { useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import Axios from "./Axios";
import Layout from "./utils/Layout/Layout";
import Blogs from "./pages/Blogs";
import Detail from "./pages/Detail";
import Signup from "./pages/Auth/Signup";
import ForgotPassword from "./pages/Auth/ForgotPassword";
import Login from "./pages/Auth/Login";
import Personalinfo from "./pages/Profile/Personalinfo";
import MyBlogs from "./pages/Profile/MyBlogs";
import ChangePassword from "./pages/Profile/ChangePassword";
import NotFound from "./pages/NotFound";

function App() {
  useEffect(() => {
    Axios.get("/blogs/list").then((res) => {
      console.log("BLOGS=>", res.data);
    });
  }, []);

  return (
    <Routes>
      {/* <Route
        path="/"
        element={
          <Layout>
            <Blogs />
          </Layout>
        }
      /> */}

      <Route path="/" element={<Layout children={<Blogs />} />} />
      <Route path="/detail/:id" element={<Layout children={<Detail />} />} />

      <Route
        path="/signup"
        element={<Layout children={<Signup />} centered />}
      />
      <Route
        path="/forgot-password"
        element={<Layout children={<ForgotPassword />} centered />}
      />
      <Route path="/login" element={<Layout children={<Login />} centered />} />

      <Route
        path="/personal-info/:id"
        element={<Layout children={<Personalinfo />} centered />}
      />
      <Route
        path="/my-blogs"
        element={<Layout children={<MyBlogs />} centered />}
      />
      <Route
        path="/change-password"
        element={<Layout children={<ChangePassword />} centered />}
      />

      <Route path="/*" element={<NotFound />} />
    </Routes>
  );
}

export default App;
