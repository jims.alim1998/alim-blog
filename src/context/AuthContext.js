import React, { createContext, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";

const AuthContext = createContext();

export const useAuthContext = () => useContext(AuthContext);

export const AuthProvider = ({ children }) => {
  const navigate = useNavigate();

  const [isLoading, setIsLoading] = useState(true);
  const [isLogin, setIsLogin] = useState(false);

  const loginHandler = () => {
    setIsLoading();
    setIsLogin(true);
    navigate("/my-blogs");
  };

  const logoutHandler = () => {
    setIsLogin(false);
    navigate("/");
  };

  return (
    <AuthContext.Provider
      value={{ isLoading, isLogin, loginHandler, logoutHandler }}
    >
      {children}
    </AuthContext.Provider>
  );
};
