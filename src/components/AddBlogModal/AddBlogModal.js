import React, { useState } from "react";
import FileUploader from "../../ui/fileUploader/FileUploader";
import MyInput from "../../ui/myInput/MyInput";
import Modal from "../../utils/Modal/Modal";
import Loader from "../../utils/Loader/Loader";

import "./addBlogModal.scss";

const _noIMG = require("../../assets/no-image.png");

const AddBlogModal = ({ visible, setVisible }) => {
  const [banner, setBanner] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  const postBlogHandler = (e) => {
    e.preventDefault();

    setIsLoading(true);
  };

  return (
    <Modal visible={visible} onCancel={() => setVisible(isLoading)}>
      <div className="addBlogModal">
        <b className="addBlogModal__title">Шинээр нийтлэх</b>

        <figure className="addBlogModal__banner">
          <img
            className={`addBlogModal__banner-img ${banner || "noIMG"}`}
            src={banner ? banner : _noIMG}
            alt="no file"
          />
          <FileUploader text="Зураг оруулах" getFile={setBanner} centered />
        </figure>

        <form className="myForm" onSubmit={postBlogHandler}>
          <div className="myForm__row">
            <label>Гарчиг:</label>
            <MyInput />
          </div>

          <div className="myForm__row">
            <label>Төрөл:</label>
            <select>
              <option value="volvo">Volvo</option>
              <option value="saab">Saab</option>
              <option value="mercedes">Mercedes</option>
              <option value="audi">Audi</option>
            </select>
          </div>

          <div className="myForm__row">
            <label>Товч тайлбар:</label>
            <textarea />
          </div>

          <div className="myForm__row">
            <label>Бичвэр:</label>
            <textarea rows={6} />
          </div>

          {isLoading ? <Loader mini /> : <button type="submit">Нийтлэх</button>}
        </form>
      </div>
    </Modal>
  );
};

export default AddBlogModal;
