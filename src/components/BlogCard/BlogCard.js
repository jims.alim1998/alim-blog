import React from "react";

import "./blogCard.scss";

const _userIcn = require("../../assets/user-icon.png");

const BlogCard = ({ id, banner }) => {
  return (
    <a href={`/detail/${id}`} className="blogCard">
      <p className="detailText">{"Дэлгэрэнгүй >>"}</p>

      <img className="blogCard__banner" src={banner} alt="no file" />

      <div className="blogCard__content">
        <figure className="blogCard__content-heading">
          <img
            className="blogCard__content-heading-icon"
            src={_userIcn}
            alt="no file"
          />

          <figcaption>
            <h3>Dragon Ball {id}</h3>
            <p className="blogCard__content-heading-date">2023/09/21</p>
          </figcaption>
        </figure>

        <p className="blogCard__content-text">
          Dragon Ball Z is a Japanese anime television series produced by Toei
          Animation. Part of the Dragon Ball media franchise, it is the
          sequel...
        </p>
      </div>
    </a>
  );
};

export default BlogCard;
