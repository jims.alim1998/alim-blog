import React, { useEffect, useState } from "react";
import SearchInput from "../ui/searchInput/SearchInput";
import BlogCard from "../components/BlogCard/BlogCard";
import Pagination from "../utils/Pagination/Pagination";
import Loader from "../utils/Loader/Loader";
import AddBlogModal from "../components/AddBlogModal/AddBlogModal";

const _banner1 = "https://images.alphacoders.com/647/647552.jpg";
const _banner2 = "https://images6.alphacoders.com/489/489471.png";

const Blogs = ({ onMyBlogs = false }) => {
  const [data, setData] = useState([]);

  const [isLoading, setIsLoading] = useState(false);
  /* initial value ni true bn, Pagination component deer ochij setIsLoading(true)-iin comment-iig boliulah */
  const [visibleAddModal, setVisibleAddModal] = useState(false);

  /* ========== Pagination  =================================== */
  const perPage = 9;
  const totalLength = 12;
  // const [totalLength, setTotalLength] = useState(0); // baazaas data-iin niit length-iig avna
  const [currentPage, setCurrentPage] = useState(1);

  const lastBlogIdx = currentPage * perPage;
  const firstBlogIdx = lastBlogIdx - perPage;
  const currentBlogs = data.slice(firstBlogIdx, lastBlogIdx);
  /* ========================================================== */

  useEffect(() => {
    setData([...Array(totalLength)].map((_, idx) => idx + 1));
  }, [totalLength]);

  return (
    <div className="blogs">
      <AddBlogModal visible={visibleAddModal} setVisible={setVisibleAddModal} />

      <div className={`blogs__heading ${onMyBlogs && "onMyBlogs"}`}>
        <div className="blogs__heading-search">
          <SearchInput />
        </div>

        {onMyBlogs && (
          <button
            className="blogs__heading-newPost outline-btn"
            onClick={() => setVisibleAddModal(true)}
          >
            Шинээр нийтлэх
          </button>
        )}
      </div>

      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className="blogs__list">
            {currentBlogs.map((item, idx) =>
              idx % 2 === 0 ? (
                <BlogCard key={idx} id={item} banner={_banner1} />
              ) : (
                <BlogCard key={idx} id={item} banner={_banner2} />
              )
            )}
          </div>

          <Pagination
            totalPage={totalLength}
            perPage={perPage}
            currentPage={currentPage}
            setCurrentPage={setCurrentPage}
            setIsLoading={setIsLoading}
            /*============================*/
            isLoading={isLoading}
          />
        </>
      )}
    </div>
  );
};

export default Blogs;
