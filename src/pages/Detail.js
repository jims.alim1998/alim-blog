import React from "react";
import { useParams } from "react-router-dom";

const _userIcn = require("../assets/user-icon.png");
const _banner1 = "https://images.alphacoders.com/647/647552.jpg";
const _banner2 = "https://images6.alphacoders.com/489/489471.png";

const Detail = () => {
  const { id } = useParams();

  return (
    <div className="detail">
      <h2 className="detail__title">Dragon Ball {id % 2 === 0 ? "Z" : "GT"}</h2>

      <figure className="detail__heading">
        <img className="detail__heading-icon" src={_userIcn} alt="no file" />

        <figcaption className="detail__heading-info">
          <span className="detail__heading-info-name">
            B. Username
            <p className="detail__heading-info-date onMobile">2023/09/21</p>
          </span>

          <p className="detail__heading-info-date">2023/09/21</p>
          <b className="detail__heading-info-type">Anime</b>
        </figcaption>
      </figure>

      <img
        className="detail__banner"
        src={id % 2 === 0 ? _banner1 : _banner2}
        alt="no file"
      />

      <p className="detail__text">
        Laborum id non mollit reprehenderit occaecat cillum eiusmod laborum.
        Ullamco sit nisi consectetur eiusmod aute. Aliquip ut aliqua nostrud qui
        incididunt magna officia tempor sit sunt. Occaecat laboris ipsum
        occaecat eu. Exercitation exercitation ad qui excepteur deserunt fugiat.
        Ex ea ad aliqua adipisicing id ad sit non velit sint ipsum in ipsum
        pariatur. Consectetur magna consequat fugiat labore aliquip consequat
        proident. Elit cupidatat ad anim labore ut. Incididunt proident non
        consequat est ipsum sunt et magna. Enim culpa culpa voluptate proident
        labore et velit aliqua. Consectetur minim ipsum incididunt sunt
        consectetur esse cupidatat adipisicing sunt ipsum sit occaecat laborum.
        Id reprehenderit id aliquip occaecat deserunt. Veniam mollit irure nulla
        cupidatat ut tempor aliqua duis. Officia sint enim ullamco dolor aute
        velit consequat ipsum eu. Elit exercitation ad irure in commodo
        voluptate ullamco velit ex nulla laborum dolore. Duis aute eiusmod
        consequat consequat ad. Veniam et enim aliquip deserunt commodo nisi est
        ex irure non eiusmod consectetur. Commodo ipsum ut esse magna fugiat
        reprehenderit deserunt ut est minim laboris do labore velit. Et duis
        exercitation tempor cillum voluptate irure laboris et est elit
        adipisicing incididunt. Sunt mollit nulla anim esse incididunt aliqua.
        Tempor sit nisi ea mollit in ex exercitation. Veniam tempor laborum
        occaecat incididunt et aliqua ea exercitation laborum sint tempor. Velit
        proident in quis aliqua ut fugiat ipsum tempor cillum occaecat aliqua
        non. Mollit minim nostrud qui aliqua veniam minim id irure tempor aute
        eu ut. Ut fugiat laborum enim enim enim do exercitation mollit. Id
        eiusmod officia id adipisicing qui. Labore anim dolor elit cillum amet
        reprehenderit aliquip. Irure non officia anim laborum sint non. Irure
        esse ipsum pariatur aliquip aute in fugiat ut ipsum minim consectetur
        dolore et. Labore dolore excepteur Lorem in aliqua amet sunt do in
        incididunt excepteur duis. Aliqua adipisicing consequat veniam ut labore
        qui mollit velit duis elit. Duis do cupidatat officia nisi proident anim
        minim. Enim non labore do reprehenderit dolor ea quis voluptate nulla
        sunt tempor excepteur duis. Id commodo consequat dolore est elit enim
        sunt adipisicing cillum ad nisi deserunt culpa Lorem. Eiusmod eiusmod
        est anim elit proident mollit id voluptate nisi. Occaecat exercitation
        ullamco sint adipisicing cupidatat. Voluptate sint consectetur sunt quis
        amet culpa. Tempor incididunt magna aliqua incididunt id sunt eu do
        reprehenderit in irure Lorem ipsum. Aliquip anim non do eiusmod ea culpa
        enim commodo reprehenderit Lorem cupidatat ad labore non. Est ipsum
        veniam et consequat. Fugiat id nulla Lorem fugiat labore non enim et
        incididunt. Proident ad mollit aliquip consectetur do ea irure
        exercitation aliqua tempor ipsum mollit. Exercitation est nulla cillum
        cupidatat in fugiat adipisicing aute consequat duis commodo. Nisi minim
        fugiat ad deserunt cillum consequat sunt minim est mollit pariatur
        ullamco. Id non sit minim occaecat incididunt voluptate qui sit dolor.
        Officia commodo exercitation voluptate dolore Lorem nisi eiusmod
        consequat qui.
      </p>
    </div>
  );
};

export default Detail;
