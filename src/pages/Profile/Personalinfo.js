import React from "react";
import { BsCameraFill } from "react-icons/bs";
import MyInput from "../../ui/myInput/MyInput";

const _noIMG = require("../../assets/no-image.png");

const Personalinfo = () => {
  const saveHandler = (e) => {
    e.preventDefault();
  };

  return (
    <div className="profile">
      <figure className="profile__avatar">
        <img className="profile__avatar-img" src={_noIMG} alt="no file" />

        <button className="profile__avatar-uploadBtn">
          <BsCameraFill />
        </button>
      </figure>

      <form className="myForm" onSubmit={(e) => saveHandler(e)}>
        {/* "Хоосон байна!" */}
        <span className="myForm__row">
          <label className="myForm__row-label">Овог:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Нэр:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Утасны дугаар:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">И-мэйл хаяг:</label>
          <MyInput />
        </span>

        <button className="profile__button" type="submit">
          Хадгалах
        </button>
      </form>
    </div>
  );
};

export default Personalinfo;
