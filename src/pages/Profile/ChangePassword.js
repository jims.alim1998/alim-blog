import React from "react";
import MyInput from "../../ui/myInput/MyInput";

const _noIMG = require("../../assets/no-image.png");

const ChangePassword = () => {
  const saveHandler = (e) => {
    e.preventDefault();
  };

  return (
    <div className="profile">
      <figure className="profile__avatar">
        <img className="profile__avatar-img" src={_noIMG} alt="no file" />
      </figure>

      <form className="myForm" onSubmit={(e) => saveHandler(e)}>
        {/* "Хоосон байна!" */}
        <span className="myForm__row">
          <label className="myForm__row-label">Хуучин нууц үг:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Шинэ нууц үг:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Шинэ нууц үг давтах:</label>
          <MyInput />
        </span>

        <button className="profile__button" type="submit">
          Хадгалах
        </button>
      </form>
    </div>
  );
};

export default ChangePassword;
