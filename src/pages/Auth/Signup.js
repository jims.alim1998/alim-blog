import React from "react";
import { Link } from "react-router-dom";
import { useFormik } from "formik";
import * as Yup from "yup";
import MyInput from "../../ui/myInput/MyInput";

const _logo = require("../../assets/logo.png");

const Signup = () => {
  const signupSchema = Yup.object().shape({
    firstname: Yup.string()
      .matches(/^[a-z\wа-яA-Z\wА-Я]*$/, "Зөвхөн үсэг бичнэ үү!")
      .matches(/^[A-Z\wА-Я][a-z\wа-я0-9_-]*$/, "Эхний үсэг том байх ёстой!")
      .required("Хоосон байна!"),
    lastname: Yup.string()
      .matches(/^[a-z\wа-яA-Z\wА-Я]*$/, "Зөвхөн үсэг бичнэ үү!")
      .matches(/^[A-Z\wА-Я][a-z\wа-я0-9_-]*$/, "Эхний үсэг том байх ёстой!")
      .required("Хоосон байна!"),
    phonenumber: Yup.string()
      .matches(/^[0-9]*$/, "Зөвхөн тоо бичнэ үү!")
      .min(8, "Урт багадаа 8 байх ёстой!")
      .required("Хоосон байна!"),
    email: Yup.string()
      .email("Зөв форматаар бичнэ үү!")
      .matches(/[a-z0-9]+@[a-z]+\.[a-z]{2,3}/, "Зөв форматаар бичнэ үү!")
      .required("Хоосон байна!"),
    password: Yup.string()
      .min(8, "Хамгийн багадаа 8 тэмдэгт байх ёстой!")
      .required("Хоосон байна!"),
    repassword: Yup.string()
      .min(8, "Хамгийн багадаа 8 тэмдэгт байх ёстой!")
      .oneOf([Yup.ref("password")], "Таарахгүй байна!")
      .required("Хоосон байна!"),
  });

  const formik = useFormik({
    initialValues: {
      firstname: "",
      lastname: "",
      phonenumber: "",
      email: "",
      password: "",
      repassword: "",
    },
    validationSchema: signupSchema,
    onSubmit: () => signupHandler(),
  });

  const signupHandler = () => {
    alert("OK");
  };

  return (
    <div className="authAccount">
      <img className="authAccount__logo" src={_logo} alt="no file" />

      <b className="authAccount__title">Бүртгүүлэх</b>

      <form className="myForm" onSubmit={formik.handleSubmit}>
        <span className="myForm__row">
          <label className="myForm__row-label">Овог:</label>
          <MyInput
            name="firstname"
            value={formik.values.firstname}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.firstname}
            errorText={formik.errors.firstname}
          />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Нэр:</label>
          <MyInput
            name="lastname"
            value={formik.values.lastname}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.lastname}
            errorText={formik.errors.lastname}
          />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Утасны дугаар:</label>
          <MyInput
            name="phonenumber"
            value={formik.values.phonenumber}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.phonenumber}
            errorText={formik.errors.phonenumber}
          />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">И-мэйл хаяг:</label>
          <MyInput
            name="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.email}
            errorText={formik.errors.email}
          />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Нууц үг:</label>
          <MyInput
            name="password"
            value={formik.values.password}
            type="password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.password}
            errorText={formik.errors.password}
          />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Нууц үг давтах:</label>
          <MyInput
            name="repassword"
            value={formik.values.repassword}
            type="password"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            touched={formik.touched.repassword}
            errorText={formik.errors.repassword}
          />
        </span>

        <button className="authAccount__button" type="submit">
          Бүртгүүлэх
        </button>

        <span className="authAccount__bottom single">
          <Link to="/login">Нэвтрэх</Link>
        </span>
      </form>
    </div>
  );
};

export default Signup;
