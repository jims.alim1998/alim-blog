import React from "react";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../context/AuthContext";
import MyInput from "../../ui/myInput/MyInput";

const _logo = require("../../assets/logo.png");

const Login = () => {
  const { loginHandler } = useAuthContext();

  const loginOnSubmit = (e) => {
    e.preventDefault();

    loginHandler();
  };

  return (
    <div className="authAccount">
      <img className="authAccount__logo" src={_logo} alt="no file" />

      <b className="authAccount__title">Нэвтрэх</b>

      <form className="myForm" onSubmit={(e) => loginOnSubmit(e)}>
        {/* Хоосон байна! */}
        <MyInput placeholder="И-мэйл" />
        <MyInput placeholder="Нууц үг" />
        <button className="authAccount__button" type="submit">
          Нэвтрэх
        </button>
        <span className="authAccount__bottom">
          <Link to="/forgot-password">Нууц үгээ мартсан</Link>
          <Link to="/signup">Бүртгүүлэх</Link>
        </span>
      </form>
    </div>
  );
};

export default Login;
