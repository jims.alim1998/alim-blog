import React from "react";
import { Link } from "react-router-dom";
import MyInput from "../../ui/myInput/MyInput";

const _logo = require("../../assets/logo.png");

const ForgotPassword = () => {
  const forgotPasswordHandler = (e) => {
    e.preventDefault();
  };

  return (
    <div className="authAccount">
      <img className="authAccount__logo" src={_logo} alt="no file" />

      <b className="authAccount__title">Нууц үг сэргээх</b>

      <form className="myForm" onSubmit={(e) => forgotPasswordHandler(e)}>
        {/* Хоосон байна! */}
        <span className="myForm__row">
          <label className="myForm__row-label">И-мэйл хаяг:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Шинэ нууц үг:</label>
          <MyInput />
        </span>

        <span className="myForm__row">
          <label className="myForm__row-label">Нууц үг давтах:</label>
          <MyInput />
        </span>

        <button className="authAccount__button" type="submit">
          Сэргээх
        </button>

        <span className="authAccount__bottom">
          <Link to="/login">Нэвтрэх</Link>
          <Link to="/signup">Бүртгүүлэх</Link>
        </span>
      </form>
    </div>
  );
};

export default ForgotPassword;
